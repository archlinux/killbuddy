mod args;
use crate::args::Args;

use std::io::Write;
use std::{env, io};

use time::macros::format_description;
use time::{Duration, OffsetDateTime};

use anyhow::{bail, Context, Result};
use clap::Parser;
use env_logger::Env;
use log::{debug, error, info, warn};
use serde::Deserialize;

use reqwest::Client;

use keycloak::types::UserRepresentation;
use keycloak::{KeycloakAdmin, KeycloakAdminToken};

use gitlab::api::users::ExternalProvider;
use gitlab::api::AsyncQuery;
use gitlab::GitlabBuilder;

#[derive(Debug, Deserialize)]
pub struct GitLabUser {
    pub id: u64,
    pub username: String,
    pub name: String,
    pub email: Option<String>,
    pub created_at: String,
    pub current_sign_in_ip: Option<String>,
    pub last_sign_in_ip: Option<String>,
}

async fn run(args: Args) -> Result<()> {
    println!("kill {}", args.username);

    let username = &env::var("KILLBUDDY_KEYCLOAK_USERNAME")
        .context("Missing env var KILLBUDDY_KEYCLOAK_USERNAME")?;
    let password = &env::var("KILLBUDDY_KEYCLOAK_PASSWORD")
        .context("Missing env var KILLBUDDY_KEYCLOAK_PASSWORD")?;
    let realm = &env::var("KILLBUDDY_KEYCLOAK_REALM")
        .context("Missing KILLBUDDY_KEYCLOAK_REALM env var")?;
    let url =
        &env::var("KILLBUDDY_KEYCLOAK_URL").context("Missing KILLBUDDY_KEYCLOAK_URL env var")?;
    let gitlab_token =
        &env::var("KILLBUDDY_GITLAB_TOKEN").context("Missing env var KILLBUDDY_GITLAB_TOKEN")?;

    let keycloak_client = Client::new();

    info!(
        "acquire API token for keycloak {} using realm {}",
        url, realm
    );

    let keycloak_token =
        KeycloakAdminToken::acquire(url, username, password, &keycloak_client).await?;
    let admin = KeycloakAdmin::new(url, keycloak_token, keycloak_client);

    let gitlab_client = GitlabBuilder::new("gitlab.archlinux.org", gitlab_token)
        .build_async()
        .await?;

    let users = admin
        .realm_users_get(
            realm,
            None,
            None,
            None,
            None,
            Some(true),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            Some(args.username.to_string()),
        )
        .await?;

    if users.len() != 1 {
        bail!("Found {} users, aborting...", users.len());
    }

    let keycloak_user = users.first().unwrap();

    let keycloak_id = keycloak_user.id.as_ref().unwrap().to_string();
    let keycloak_username = keycloak_user.username.as_ref().unwrap().to_string();
    let keycloak_created = keycloak_user.created_timestamp.unwrap();
    let keycloak_created = OffsetDateTime::from_unix_timestamp(
        Duration::milliseconds(keycloak_created).whole_seconds(),
    )?;
    let date_format = format_description!("[year]-[month]-[day] [hour]:[minute]:[second]");

    println!("keycloak");
    println!("-       id: {}", &keycloak_id);
    println!("- username: {}", &keycloak_username);
    println!("-  enabled: {}", keycloak_user.enabled.as_ref().unwrap());
    println!(
        "-     name: {} {}",
        keycloak_user.first_name.as_ref().unwrap(),
        keycloak_user.last_name.as_ref().unwrap()
    );
    println!("-    email: {}", keycloak_user.email.as_ref().unwrap());
    println!(
        "- verified: {}",
        keycloak_user.email_verified.as_ref().unwrap()
    );
    println!("-  created: {}", keycloak_created.format(&date_format)?);

    if keycloak_username != args.username.as_str() {
        bail!("Username mismatch, aborting...");
    }

    let users_endpoint = gitlab::api::users::Users::builder()
        .external_provider(
            ExternalProvider::builder()
                .uid(&keycloak_username)
                .name("saml")
                .build()
                .unwrap(),
        )
        .active(())
        .build()
        .unwrap();
    let users: Vec<GitLabUser> = users_endpoint.query_async(&gitlab_client).await?;
    if users.is_empty() {
        stdin_confirm()?;
        keycloak_disable(&admin, realm, &keycloak_id, keycloak_user).await?;
        warn!("Failed to query GitLab user for {}", &keycloak_username);
        return Ok(());
    } else if users.len() > 1 {
        bail!(
            "Somehow got {} GitLab user results for {}",
            users.len(),
            keycloak_username
        )
    }

    let gitlab_user = users
        .first()
        .with_context(|| format!("Failed to query GitLab user for {}", keycloak_username))?;
    debug!(
        "Successfully retrieved user {} to GitLab id {}",
        gitlab_user.username, gitlab_user.id
    );
    if keycloak_username != gitlab_user.username {
        bail!(
            "Username mismatch between keycloak and GitLab: {} vs {}",
            keycloak_username,
            gitlab_user.username
        );
    }

    println!("gitlab");
    println!("-       id: {}", gitlab_user.id);
    println!("- username: {}", gitlab_user.username);
    println!("-    email: {}", gitlab_user.email.as_ref().unwrap());
    println!("-  created: {}", gitlab_user.created_at);

    stdin_confirm()?;
    keycloak_disable(&admin, realm, &keycloak_id, keycloak_user).await?;
    gitlab_delete(gitlab_token, gitlab_user.id).await?;

    Ok(())
}

fn stdin_confirm() -> Result<()> {
    let mut user_input = String::new();
    let stdin = io::stdin();
    print!("delete user? [y/n] ");
    io::stdout().flush()?;
    stdin.read_line(&mut user_input)?;
    if user_input.trim() != "y" {
        bail!("Confirmation failed, aborting...")
    }
    Ok(())
}

async fn keycloak_disable(
    admin: &KeycloakAdmin,
    realm: &str,
    id: &str,
    rep: &UserRepresentation,
) -> Result<()> {
    info!("kill keycloak sessions for user {}", id);
    admin.realm_users_with_id_logout_post(realm, id).await?;

    info!("disable keycloak user {}", id);
    let mut rep = rep.clone();
    rep.enabled = Some(false);
    admin.realm_users_with_id_put(realm, id, rep).await?;

    Ok(())
}

async fn gitlab_delete(token: &str, uid: u64) -> Result<()> {
    info!("delete GitLab user {}", uid);
    let client = Client::new();
    let response = client
        .delete(format!(
            "https://gitlab.archlinux.org/api/v4/users/{uid}?hard_delete=true",
            uid = uid
        ))
        .bearer_auth(token)
        .send()
        .await?;
    if !response.status().is_success() {
        bail!("Failed to delete user {}", uid);
    }
    Ok(())
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    let logging = match args.verbose {
        0 => "info",
        1 => "killbuddy=debug",
        2 => "debug",
        _ => "trace",
    };

    env_logger::init_from_env(Env::default().default_filter_or(logging));

    if let Err(err) = run(args).await {
        error!("Error: {:?}", err);
        for cause in err.chain() {
            error!("Caused by: {:?}", cause)
        }
        std::process::exit(1)
    }
}
